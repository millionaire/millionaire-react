import React             from 'react'
import { Provider }      from 'react-redux'
import { render }        from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import thunk             from 'redux-thunk'
import App               from './components/App'
import reducer           from './reducers/index'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { check_token }   from './actions/auth'
import                        './style/index.css'
import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'
import registerServiceWorker from './registerServiceWorker';

const persistConfig = {
  key: 'root',
  storage,
}

export const rest_api_host = 'https://ncmillionaire.ddns.net:8080'
// export const rest_api_host = 'http://localhost:8080'

// localStorage.clear() // debug only, delete in production
const persistedReducer = persistReducer(persistConfig, reducer)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(thunk)))
let persistor = persistStore(store)
store.dispatch(check_token())

render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </PersistGate>
  </Provider>
  ,document.getElementById('root')
)
registerServiceWorker();
