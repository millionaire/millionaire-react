import React from 'react';
import { connect } from 'react-redux'
import Admin from '../containers/admin/Admin'
import RoomList from '../containers/RoomList'

const Root = ({role}) => {
if (role === 'admin')
	return <Admin  />
   else
	return <RoomList  />
  
} 

const mapStateToProps = state => ({
  role: state.role,
})

export default connect(mapStateToProps)(Root)
