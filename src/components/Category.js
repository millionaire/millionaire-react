import React from 'react';

const Category = ({ index, sum, className, active}) => 
	<tr className = {className}>
		<td>{index}</td>
		<td>{index<=active ? String.fromCharCode(9830): ' ' }</td>
		<td>{sum}</td>
	</tr>

export default Category

//