import React from 'react';
import Cont5050 from '../containers/Cont5050'
import ContPoll from '../containers/ContPoll'
import ContTimer from '../containers/ContTimer'
import '../style/lifeline.css'

const Lifelines = () => 
  <ul className = 'lifeline'>
    <Cont5050 />
    <ContTimer />
    <ContPoll />
  </ul>

export default Lifelines
