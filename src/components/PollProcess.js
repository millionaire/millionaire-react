import React from 'react';
import '../style/pollProc.css'

var PollProcess = React.createClass({
    getInitialState: function(){
        return { elapsed: this.props.start };
    },

    componentDidMount: function(){
        this.timer = setInterval(this.tick, 1000);        
    },

    componentWillUnmount: function(){
         clearInterval(this.timer);
    },

    tick: function(){
        this.setState({elapsed: (this.state.elapsed >= 0) ? this.state.elapsed-1: -1});
    },

    render: function() {
        var elapsed =  this.state.elapsed ;
        if (this.props.used) {
         return (
            <div className="pollProc" style = {(this.state.elapsed < 0) ? {display: 'none'}: {}}>
                <p>Пожалуйста, выберите на своем устройстве правильный ответ {elapsed}</p>
            </div>
         )
     }
        else {
            return <div></div>
        }
    }
});


 export default PollProcess;