import React from 'react';
import '../style/poll.css'

const HintPoll = ({question2, used, used5050}) => {

  const Rect = ({x,value, text}) => {
    var Animate = ({value}) => {
      return (
        <animate attributeName="height" from="0" to={used ? value : 0} dur="1s" fill="freeze" />
      )
    }
      // Rounding polyfill
    (function() {
      /**
       * Decimal adjustment of a number.
       *
       * @param {String}  type  The type of adjustment.
       * @param {Number}  value The number.
       * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
       * @returns {Number} The adjusted value.
       */
      function decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
          return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (value === null || isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
          return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
      }

      // Decimal round
      if (!Math.round10) {
        Math.round10 = function(value, exp) {
          return decimalAdjust('round', value, exp);
        };
      }
    })();

    return (
      <g>
        <text
          className="tick"
          x = {x + 10}
          y = "110%"
          // fontSize = "35"
        >
          {text}
        </text>
        <rect
          x={x}
          y="0"
          width="30"
          height="0"
          transform="scale(1,-1) translate(0,-200)"
        >
          <Animate value={value*2}/>
        </rect>
        <text className="pollPercent" x = {x + 15} y = "55%" fontSize = '75%'>
          {Math.round10(value,-1)}%
        </text>
      </g>
    )
  }

  if (used)
    return(
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
        width="400" height="400" viewBox="0 0 400 200" className = {'hintPollChart'}>
        <g>
          <Rect x =  {50} value={question2.answers[0].percent} text={'A'}/>
          <Rect x =  {90} value={question2.answers[1].percent} text={'B'}/>
          <Rect x = {130} value={question2.answers[2].percent} text={'C'}/>
          <Rect x = {170} value={question2.answers[3].percent} text={'D'}/>
        </g>
        </svg>
      )
  else
    return <div></div>

  // return (
  //           <div style={ {width: '50%'} }>
  //               <BarChart ylabel={used ? '' : null}
  //                 width={used ? 300 : null}
  //                 height={used ? 300 : null}
  //                 margin={{top: 20, right: 100, bottom: 30, left: 40}}
  //                 data={[ {text: 'A', value: question2.answers[0].percent },
  //                         {text: 'B', value: question2.answers[1].percent },
  //                         {text: 'C', value: question2.answers[2].percent },
  //                         {text: 'D', value: question2.answers[3].percent }]}
  //                 // onBarClick={this.handleBarClick}/>
  //                 />
  //           </div>

  //   )

}
export default HintPoll



