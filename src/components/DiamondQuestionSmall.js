import React from 'react';
import '../style/question.css'


const DiamondQuestionSmall = ({text}) => {

  console.log(text.length)
  // let questionSize = text.legth > 40 ? 2 : 3
  let questionSize = 2.5
  let questionTextStyle = {
    color: 'white',
    // fontSize: text.legth > 40 ? 0.1 : 2,
    // fontSize: 0.1,
    margin: 'auto',
    fontSize: questionSize,
    textAlign: 'center',
  }
  return(
    <svg viewBox = '0 0 64 12'>
      <linearGradient id="svgid_1" >
        <stop  offset="0" style={{stopColor:"#0B4B9F"}} />
        <stop  offset="1" style={{stopColor:"#C1E8E5"}} />
      </linearGradient>

      <path
        stroke="url(#svgid_1)"
        strokeWidth="0.2"
        d='M0 6 C2 4 4 0 8 0 L56 0 C60 0 62 4 64 6 C62 8 60 12 56 12 L8 12 C4 12 2 8 0 6'
      />

      <foreignObject x="10%" y="10%" width="80%" height="10" >
            <div style={{height:  '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <p style={questionTextStyle}>{text}</p>
            </div>
          </foreignObject>
    </svg>
  )
}

export default DiamondQuestionSmall

