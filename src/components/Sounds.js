import React from 'react';
import Sound from 'react-sound';
import MediaQuery from 'react-responsive'

class Sounds extends React.Component {
	state = {
		url: this.props.url
	}

	componentWillReceiveProps(nextProps){
		this.setState({url:nextProps.url})
	}

	render() {
		console.log(this.state)
		return(
			<MediaQuery minWidth={600}>
			<Sound
				url = {this.props.url}
				autoLoad = {true}
				playStatus = {this.props.status || Sound.status.PLAYING}
				volume = {this.props.volume || 100}
				playFromPosition = {0}
				loop = {this.props.loop || false}
				onFinishedPlaying={this.props.onFinishedPlaying}
			/>
		</MediaQuery>
		)
	}
}

export default Sounds;
