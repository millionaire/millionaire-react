import React from 'react'
import { connect } from 'react-redux'
import Reset      from '../containers/Reset'

const Private = ({roomId}) => {
return (
<div  >
    <div style={{'color':'white','font-size': '40px','position':'absolute'}}>
          <p>roomId: { roomId }</p>
    </div>
 <Reset/>
 </div>
 )
} 

const mapStateToProps = state => { 
  return {
    roomId: state.roomId
  }
}


export default connect(mapStateToProps)(Private)
