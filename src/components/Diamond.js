import React from 'react';
import KeyHandler, {KEYPRESS} from 'react-key-handler';


const Diamond = ({ userId, fill, text, blinker, letter, onClick , className, isActive, style, textstyle }) => {
  return(
    <div style={style}>
      {letter !== undefined && letter !== null && <KeyHandler keyEventName={KEYPRESS} keyValue={letter.toLowerCase()} onKeyHandle={onClick} />}
      <svg viewBox="0 0 393 50"
        style={style}
        className = {className }
        onClick={onClick}
        >
          <linearGradient id="SVGID_1_">
            <stop offset="0" style={{stopColor:"#0B4B9F"}} />
            <stop offset="1" style={{stopColor:"#C1E8E5"}} />
          </linearGradient>

          <path style={blinker ? {animation: 'blinkeranimation 0.5s steps(1) infinite'} : {}}
            stroke="url(#SVGID_1_)"
            strokeWidth="1"
            fill={fill}
            d="M1.5,25.86
            c6.388,4.04,21.2,23.368,30.858,23.71
            l328.492,0.082
            c9.66-0.342,24.262-19.964,30.65-24.005
            v-0.356c-6.388-4.04-21.199-23.367-30.858-23.709
            L32.149,1.5
            C22.49,1.842,7.888,21.464,1.5,25.505V25.86z"
          />

          <text
              x="30" y="32"
              fontSize="20"
              textAnchor="middle"
              fill="#df8900"
          >
          { letter !== undefined ? letter + ': ' : ''}
          </text>

          <text
            x="200" y="32"
            fontSize="20"
            className={textstyle}
            textAnchor="middle"
            fill="white"
          >
          {isActive ? text : null }
         </text>
        </svg>
      </div>
  )
}

export default Diamond
