import React from 'react';
import KeyHandler, {KEYPRESS} from 'react-key-handler';

const Answer = ({ userId, activePlayerId, text, letter, onClick , className, isActive,style, textstyle, logged_in }) => 
	
    <div onClick={ isActive && logged_in ? onClick : null} 
		style={style} 
		className = {'answer ' + className }
	> 
          {letter != null && <KeyHandler keyEventName={KEYPRESS} keyValue={letter.toLowerCase()} onKeyHandle={isActive && logged_in ? onClick : null} />}

		<p> { letter !== null ? <span>{letter + ': '}</span> : <div></div>}<div className={textstyle} style={{'display': 'inline'}}>{ isActive ? text : null }</div></p>
	</div>

export default Answer
