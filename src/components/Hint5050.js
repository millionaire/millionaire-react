import React from 'react'

const Hint5050 = ({ fetchQuestionHint5050, used }) => {
  return (
    <li onClick={!used ? fetchQuestionHint5050 : null } className={!used ?'fiftyfifty': 'used5050'}></li>
  )
}

export default Hint5050
