import React from 'react'

const ButtonPoll = ({ fetchQuestionHintPoll, used }) => {
	console.log('used '+ used)
  return (
    <li onClick={!used ? fetchQuestionHintPoll : null} className={!used ? 'audience': 'usedAudience'}></li>
  )
}

export default ButtonPoll
