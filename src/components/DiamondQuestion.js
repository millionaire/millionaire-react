import React from 'react';


const DiamondQuestion = ({ text }) => {
  let questionSize = 12

  let questionTextStyle = {
    color: 'white',
    // fontSize: text.legth > 40 ? 0.1 : 2,
    // fontSize: 0.1,
    margin: 'auto',
    fontSize: questionSize,
    textAlign: 'center',
  }
  return(
      <svg viewBox='0 0 393 50'>
          <linearGradient id="SVGID_1_">
            <stop offset="0" style={{stopColor:"#0B4B9F"}} />
            <stop offset="1" style={{stopColor:"#C1E8E5"}} />
          </linearGradient>

          <path
            stroke="url(#SVGID_1_)"
            strokeWidth="1"
            d="M1.5,25.86
            c6.388,4.04,21.2,23.368,30.858,23.71
            l328.492,0.082
            c9.66-0.342,24.262-19.964,30.65-24.005
            v-0.356c-6.388-4.04-21.199-23.367-30.858-23.709
            L32.149,1.5
            C22.49,1.842,7.888,21.464,1.5,25.505V25.86z"
          />

          <foreignObject x="10%" y="10%" width="80%" height="40" >
            <div style={{height:  '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <p style={questionTextStyle}>{text}</p>
            </div>
          </foreignObject>

        </svg>
  )
}

export default DiamondQuestion
