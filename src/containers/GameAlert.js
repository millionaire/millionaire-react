import React         from 'react';
import { connect }   from 'react-redux';
import Stats from './Stats'
import {
    continueGame,
    anotherPlayer
                   } from '../actions/game_fetches'
const GameAlert = ({newPlayer, isHost, showPlayerAlert, showGameOverWonAlert, showNoMorePlayerAlert, continueGame, anotherPlayer, gameStatus}) => {
    if (showNoMorePlayerAlert) {
        return (<div className='newPlayerAlert'>
            <p>Нет кандидатов на роль активного игрока</p>
            <Stats />
        </div>)
    }
    else if (showPlayerAlert)
        return (<div className='newPlayerAlert'>
            Current active player is {newPlayer}. 
            {
                isHost && <button className='green_button' onClick={continueGame}>Continue</button>
            }
            {
                isHost &&  <button className='green_button' onClick={anotherPlayer}>Another player</button>
            }

        </div>)
    else if (showGameOverWonAlert) {
        return (<div className='newPlayerAlert'>
            <p>Game Over!</p>
            <Stats />
        </div>)
    }
    else
        return null
}
const mapStateToProps = state => ({
  showPlayerAlert: state.showPlayerAlert,
  showGameOverWonAlert: state.showGameOverWonAlert,
  newPlayer: state.activePlayerId >= 0 && state.roomUserList.find(user => user.id === state.activePlayerId) !== undefined && state.roomUserList.find(user => user.id === state.activePlayerId).name,
  showNoMorePlayerAlert: state.showNoMorePlayerAlert,
  isHost: state.userId === state.hostId,
})

const mapDispatchToProps = dispatch => { 
  return {
    continueGame: () => dispatch(continueGame()),
    anotherPlayer:() => dispatch(anotherPlayer()),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(GameAlert);
