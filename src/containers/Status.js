import React from 'react';
import { connect } from 'react-redux'
import Typography from 'material-ui/Typography'

const Status = ({status, activePlayerUsername}) => {
  return (
    <div className='playerStatus'>
      <Typography variant="body1" >{'статус: ' + status}</Typography>
      <Typography variant="body1" >активный игрок: { status === 'active'? null : activePlayerUsername }</Typography>
    </div>
  )
}


const mapStateToProps = state => {
  let activePlayer = state.roomUserList.filter(u => u.id === state.activePlayerId)[0]
  let activePlayerUsername = activePlayer ? activePlayer.name : 'не выбран'
  return {
    status: state.userId === state.activePlayerId ? 'active':  state.userId === state.hostId?'host':'player',
    activePlayerUsername,
  }
}

export default connect(mapStateToProps)(Status)
