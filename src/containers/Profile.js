import React from 'react'
import { connect } from 'react-redux'
import '../style/profile.css'
import loadMoney from '../actions/loadMoney'
import loadGamesHistory from '../actions/loadGamesHistory'

class Profile extends React.Component {
  componentWillMount() {
    this.props.loadMoney(this.props.userId)
    this.props.loadGamesHistory()
  }

  render() {
    return (
      <div className='profilePage'>
        <h1>имя: {this.props.username}</h1>
        <h1>роль: {this.props.role}</h1>
        <h1>деньги: {this.props.money}</h1>
        <hr/>
        <h1>История игр</h1>
        <table>
        <thead>
          <tr>
            <th>комната</th>
            <th>выигрыш</th>
          </tr>
        </thead>
        <tbody>{
          this.props.gamesHistory.reverse().map(game =>
            <tr key={game.id} >
              <td>{game.roomId}</td>
              <td>{game.money}</td>
            </tr>
          )
        }
        </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = state => { 
  return {
    username: state.username,
    role: state.role,
    money: state.money,
    userId: state.userId,
    gamesHistory: state.gamesHistory,
  } 
}

const mapDispatchToProps = dispatch => { 
  return {
    loadMoney: userId => dispatch(loadMoney(userId)),
    loadGamesHistory: () => dispatch(loadGamesHistory()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
