import React 		 from 'react';
import { connect }   from 'react-redux';
import FetchScore    from '../containers/FetchScore'
import Lifelines     from '../components/Lifelines'

const StatusBar = ({isPreliminary}) => {
	if (isPreliminary)
		return <div></div>
	else {
		return (
	      <div className = "status">
	        <Lifelines />
	        <FetchScore />
	      </div>
	    )
	}
}

const mapStateToProps = state => ({
  isPreliminary: state.preliminary,
})

 export default connect(mapStateToProps)(StatusBar)
