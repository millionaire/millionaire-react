import React from 'react';
import { connect } from 'react-redux'
// import Spinner from '../components/Spinner'
import  RingLoader  from '../components/RingLoader';
 
const Spinner = ({loading}) => {
  // console.log('OUUUUUUUU loading', loading)
  if (loading)
    return (
      <div className='sweet-loading' >
        <RingLoader
          color={'#eee'} 
          loading={loading} 
        />
      </div>
    )
  else
    return null;
}


const mapStateToProps = state => ({
  loading: state.loading
})

export default connect(mapStateToProps)(Spinner)
