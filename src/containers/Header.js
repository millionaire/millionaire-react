import React      from 'react';
import LogOutButton from './LogOutButton'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import KeyHandler, {KEYPRESS} from 'react-key-handler';
import { log_out } from '../actions/auth'
import {continueGame} from '../actions/game_fetches'
import stopGameServer from '../actions/stopGame'
import toggleHeader from '../actions/toggleHeader'
import Status from './Status'
import '../style/header.css'
import MediaQuery from 'react-responsive'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Button from 'material-ui/Button'
import green from 'material-ui/colors/green';

const Header = props => {

  return props.logged_in ? (
    <MediaQuery minWidth={600}>{(matches) => {
      if (matches) { return(
        <div className='header'>
          <AppBar position="static" color="default">
            <Toolbar>
              <Link to='/'><Button size="small" color="inherit">{props.role === 'admin' ? 'Меню' : 'Комнаты'}</Button></Link>
              {
                props.joinedRoomId ?
                  props.gameStarted ?
                    <span>
                      <Link color={green[500]} to={`/room/${props.joinedRoomId}`}>
                        <Button size="small" >
                          {`комната ${props.joinedRoomId} | игра идет`}
                        </Button>
                      </Link>
                    {props.userId === props.hostId ? 
                      <div>
                          <Button
                            size="small"
                            color="secondary"
                            onClick={() => props.stopGameServer(props.joinedRoomId)}>
                                                    <Link to={'/'}>
                            остановить игру
                          
                        </Link>
                          </Button>

                        <KeyHandler keyEventName={KEYPRESS} keyValue="n" onKeyHandle={props.no_pop_ups && props.show_result ? props.continueGame : null} />
                        { props.no_pop_ups && props.show_result &&<Button className='green_button'
                        onClick={props.no_pop_ups && props.show_result ? props.continueGame : null}
                        >
                        Continue
                        </Button>}
                      </div>
                      :''
                    }
                  </span>
                :
                  <Link to={`/room/${props.joinedRoomId}`}>
                    <Button size="small">{`room ${props.joinedRoomId} | ожидание старта`}</Button>
                  </Link>
              :
                <span></span>
            }
            <Link to='/profile'><Button size="small" color="inherit">{ props.username }</Button></Link>
            <LogOutButton log_out={ props.log_out } />
            {props.role !== 'admin' && <Status />}
            <Link to='/help'><Button size="small">?</Button></Link>  
            </Toolbar>
          </AppBar>

          </div>
      )}
      else { return <div style={{color: 'white'}}>{props.username}</div>}
    }}</MediaQuery>
  )
  : (
    <AppBar position="static" color="default">
      <Toolbar>
        <Link to='/login'><Button size="small">Войти</Button></Link>
        <Link to='/signup'><Button size="small">Зарегистрироваться</Button></Link>
      </Toolbar>
    </AppBar>
  )
}

const mapStateToProps = state => {
  return {
    logged_in: state.logged_in,
    username: state.username,
    joinedRoomId: state.joinedRoomId,
    gameStarted: state.gameStarted,
    userId: state.userId,
    hostId: state.hostId,
    show_result: state.show_result,
    role: state.role,
    no_pop_ups: !(state.showPlayerAlert || state.showNoMorePlayerAlert || state.showGameOverWonAlert),
    showHeader: state.showHeader,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    log_out: () => dispatch(log_out()),
    stopGameServer: roomId => dispatch(stopGameServer(roomId)),
    continueGame: () => dispatch(continueGame()),
    toggleHeader: () => dispatch(toggleHeader()),    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
