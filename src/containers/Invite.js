import React      from 'react';
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import useInviteLink from '../actions/useInviteLink'
import '../style/waitGame.css'
import joinRoom from '../actions/joinRoom'

class Invite extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      joinButtonClicked: false,
    }
  }

  render() {
    let roomId = parseInt(this.props.match.params.room_id, 10)
    if (this.props.logged_in && this.props.joinedRoomId === roomId) {
      return <Redirect to={`/room/${roomId}`}/>
    }
    else {
      return (
        <div className='waitGame'> {
          !this.state.joinButtonClicked ?
            <button
              className='green_button'
              onClick={() => {
                this.setState({joinButtonClicked: true})
                if (!this.props.logged_in) {
                  this.props.useInviteLink(roomId, this.props.match.params.token)
                }
                else if (this.props.joinedRoomId !== roomId) {
                  this.props.joinRoom(roomId)
                }
              }}
            >
            join room
            </button>
          : <h1>Загрузка...</h1>
        }
        </div>
      )
    }
  }
}


const mapStateToProps = state => {
  return {
    logged_in: state.logged_in,
    // username: state.username,
    joinedRoomId: state.joinedRoomId,
    // gameStarted: state.gameStarted,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    joinRoom: id => dispatch(joinRoom(id)),
    useInviteLink: (roomId, inviteToken) => dispatch(useInviteLink(roomId, inviteToken))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Invite)
