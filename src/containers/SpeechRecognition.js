import React from 'react'
import { connect }                      from 'react-redux'
import { loadVoicePhrases } from '../actions/voiceCommands'
import {continueGame,
 fetchAnswer,
 fetchQuestionHint5050,
 fetchQuestionHintPoll,
 fetchQuestionHintCall} from '../actions/game_fetches'

class SpeechRecognition extends React.Component {
  componentWillMount() {
    this.props.loadVoicePhrases()
  }

  render() {

    if (window.webkitSpeechRecognition === undefined) {
      return(null)
    }

    var grammar = "#JSGF V1.0; grammar keywords; public <keywords> =  дальше | Следующий вопрос | 1 ответ | 2 ответ | 3 ответ | 4 ответ | Подсказка 50 на 50 | Подсказка звонок другу | Подсказка помощь зала;"
    var recognition = new window.webkitSpeechRecognition()
    var speechRecognitionList = new window.webkitSpeechGrammarList()
    speechRecognitionList.addFromString(grammar, 1);
    recognition.grammars = speechRecognitionList;
    recognition.lang = 'ru-RU';
    recognition.continuous = true;


    let props = this.props

    let phrases2actions = props.phrases2actions
    if (phrases2actions !== undefined) {
      for (let ph of ['первый отряд','первый ответ','1 ответа']){
        phrases2actions[ph] = '1 ответ'
      }
      for (let ph of ['второй отряд','второй ответ','2 ответа']){
        phrases2actions[ph] = '2 ответ'
      }
      for (let ph of ['третий отряд','третий ответ','3 ответа']){
        phrases2actions[ph] = '3 ответ'
      }
      for (let ph of ['четвертый отряд','четвертый ответ','4 ответа']){
        phrases2actions[ph] = '4 ответ'
      }
    }

    recognition.onresult = function(event) {

      let word = event.results[event.resultIndex][0].transcript.trim().toLowerCase();

      console.log(word)

      let action = phrases2actions[word]
      console.log(action)
      switch (action){
        case 'продолжить игру':
          props.continueGame();
          break;
        case '1 ответ':
          props.fetchAnswer(props.question.answerDTOs[0].id);
          break;
        case '2 ответ':
          props.fetchAnswer(props.question.answerDTOs[1].id);
          break;
        case '3 ответ':
          props.fetchAnswer(props.question.answerDTOs[2].id);
          break;
        case '4 ответ':
          props.fetchAnswer(props.question.answerDTOs[3].id);
          break;
        case 'подсказка 50 на 50':
          props.fetchQuestionHint5050();
          break;
        case 'подсказка помощь зала':
          props.fetchQuestionHintPoll();
          break;
        case 'подсказка звонок другу':
          props.fetchQuestionHintCall();
          break;
        default:
          break;
      }
      console.log(event);
    }
    recognition.start();
    return(null)
  }

}

const mapStateToProps = state => {
  console.log(state.voicePhrases)
  return {
    question: state.question,
    show_result: state.show_result,
    no_pop_ups: !(state.showPlayerAlert || state.showNoMorePlayerAlert || state.showGameOverWonAlert),
    phrases2actions: state.voicePhrases,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    continueGame: () => dispatch(continueGame()),
    fetchAnswer: (id) => dispatch(fetchAnswer(id)),
    fetchQuestionHint5050: () => dispatch(fetchQuestionHint5050()),
    fetchQuestionHintPoll: () => dispatch(fetchQuestionHintPoll()),
    fetchQuestionHintCall: () => dispatch(fetchQuestionHintCall()),
    loadVoicePhrases: () => dispatch(loadVoicePhrases()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SpeechRecognition)
