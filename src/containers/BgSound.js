import React from 'react'
import { connect } from 'react-redux'
import Sounds from '../components/Sounds'
import  right1 from '../asserts/music/classic_r1.mp3'


const BgSound = ({url,preliminary,requestPostsd,isHost}) => { 
	return !isHost && <Sounds url = {url} volume = {50} loop = {!preliminary } /> //onFinishedPlaying={preliminary ? requestPostsd : () => {}}
}

const mapStateToProps = state => { 
    return {
		url: (state.actSound === null || state.actSound === right1) && !state.mutebgsound  ? state.bgSound : null,
        preliminary: state.preliminary,
        isHost: state.userId === state.hostId,
	} 
}

// // по идее не юзается, TODO: delete
// const mapDispatchToProps = dispatch => { 
//     return {
//         requestPostsd: () => dispatch(requestPosts()),
//     }
// }

export default connect(mapStateToProps)(BgSound) //, mapDispatchToProps
