import { connect }                      from 'react-redux'
import { fetchOneRoom,
        fetchQuestion,
        correctAnswerReceived,
        preliminaryAnswerClicked,
        normalPlayerAnswerClicked,
        receivePostsHint5050,
        receivePostsHintPoll,
        receivePostsHintCall,
        continueGame,
        playFinalAnswer,
        skipLetsplay,
        setPrelimResults,
        newPlayerAlert,
        noMorePlayersAlert,
        resetGameAlerts,
        gameOverWonAlert,
        hintPollStarted,
        hintCallStarted,
        getOneRoom           }          from '../actions/game_fetches'
import {startGame}                      from '../actions/startGame'
import {stopGame}                       from '../actions/stopGame'
import { rest_api_host }                from '../index'
import React                            from 'react';
import SockJsClient                     from 'react-stomp';
import { hideOrShowQuestion }           from '../actions/game_fetches'
import setGameStatus                    from '../actions/setGameStatus'
import { withRouter } from 'react-router-dom'
import loadStats from '../actions/loadStats'

const WSWrapper = withRouter(props => {
    return (
      <div>
        <SockJsClient url={rest_api_host + '/gs-guide-websocket'} topics={['/topic/room' + props.roomId]}
            onMessage={(msg) => {
                if (!props.isFetching) {
                    switch (msg.wsMessage) {
                        case 'CORRECT_ANSWER':
                            props.correctAnswerReceived(msg.payload.correct, msg.payload.answerId);
                            props.hideOrShowQuestion('hide',3000)
                            if (props.userId === props.hostid) {
                              props.continueGame(9000);
                            }
                            break;
                        case 'ACTIVE_PLAYER_ANSWER_CLICKED':
                            if (props.isActive || !props.logged_in ) { //|| props.userId === props.hostId
                                props.normalPlayerAnswerClicked(msg.payload.answerId,true);
                                props.playFinalAnswer();
                            }
                            break;
                        case "SKIP_LETSPLAY":
                          props.skipLetsplay()
                          break;
                        case 'NEW_PLAYER':
                            props.correctAnswerReceived(msg.payload.correct);
                            props.getOneRoom(msg.payload.newRoomState);
                            props.newPlayerAlert();
                            // props.setGameStatus('playing')
                            break;
                        case 'NO_MORE_PLAYERS':
                            if (msg.payload !== null) {
                              props.correctAnswerReceived(msg.payload.correct);
                              props.getOneRoom(msg.payload.newRoomState);
                            }
                            props.noMorePlayersAlert();
                            break;
                        case 'CONTINUE_GAME':
                            props.fetchQuestion();
                            if (props.pop_ups_on_screen) {
                              props.resetGameAlerts();
                            }
                            break;
                        case 'GAMEOVER_WON':
                            props.correctAnswerReceived(msg.payload.correct);
                            props.gameOverWonAlert()
                            break;
                        case 'ENDPRELIMQUESTION':
                            props.setPrelimResults(msg.payload);
                            //props.preliminaryAnswerClicked(msg.answerPairs,msg.correctAnswerPairs);
                            props.fetchOneRoom(props.roomId);
                            break;
                        case 'CONTINUEPRELIMQUESTION':
                            props.fetchQuestion();
                            break;
                        case 'HINT5050USED':
                            props.receivePostsHint5050(msg.payload);
                            break;
                        case 'HINTPOLLSTARTED':
                            props.hintPollStarted();
                            break;
                        case 'HINTPOLLUSED':
                            props.receivePostsHintPoll(msg.payload);
                            break;
                        case 'HINTCALLSTARTED':
                            props.hintCallStarted();
                            break;
                        case 'HINTCALLUSED':
                            props.receivePostsHintCall();
                            break;
                        case 'GAMESTART':
                            props.fetchQuestion()
                            props.startGame()
                            break;
                        case 'STOP_GAME':
                            props.stopGame()
                            props.history.push('/')
                            break;
                        case 'ROOMSTATECHANGED':
                            props.getOneRoom(msg.payload)
                            break;
                        default:
                            break;
                    }

                }
             }}
            debug={true}/>
      </div>
    )
  })



const mapStateToProps = state => ({
    isFetching: state.isFetching,
    joinedRoomId: state.joinedRoomId,
    userId: state.userId,
    activePlayerId: state.activePlayerId,
    hostid : state.hostId,
    normal_player_click: state.normal_player_click,
    logged_in: state.logged_in,
    isActive: (state.userId === state.activePlayerId),
    pop_ups_on_screen: (state.showPlayerAlert || state.showNoMorePlayerAlert || state.showGameOverWonAlert),
})

const mapDispatchToProps = dispatch => ({
  hintPollStarted: () => dispatch(hintPollStarted()),
  continueGame: (delay) => dispatch(continueGame(delay)),
  hintCallStarted: () => dispatch(hintCallStarted()),
  fetchQuestion: () => dispatch(fetchQuestion()),
  startGame:() => dispatch(startGame()),
  fetchOneRoom: roomId => dispatch(fetchOneRoom(roomId)),
  correctAnswerReceived: (result,answerId) => dispatch(correctAnswerReceived(result,answerId)),
  receivePostsHint5050: (question) => dispatch(receivePostsHint5050(question)),
  receivePostsHintPoll: (question) => dispatch(receivePostsHintPoll(question)),
  receivePostsHintCall:() => dispatch(receivePostsHintCall()),
  getOneRoom:(room) => dispatch(getOneRoom(room)),
  preliminaryAnswerClicked: (answerArray, correctAnswerPairs) => {
    dispatch(preliminaryAnswerClicked(answerArray,correctAnswerPairs))
  },
  setPrelimResults:(arr) => dispatch(setPrelimResults(arr)),
  newPlayerAlert: () => dispatch(newPlayerAlert()),
  noMorePlayersAlert: () => dispatch(noMorePlayersAlert()),
  resetGameAlerts: () => dispatch(resetGameAlerts()),
  gameOverWonAlert: () => dispatch(gameOverWonAlert()),
  playFinalAnswer: () => dispatch(playFinalAnswer()),
  skipLetsplay: () => dispatch(skipLetsplay()),
  normalPlayerAnswerClicked: (clicked_id, play_finalanswer) => dispatch(normalPlayerAnswerClicked(clicked_id,play_finalanswer)),
  hideOrShowQuestion: (action,delay) => dispatch(hideOrShowQuestion(action,delay)),
  setGameStatus: status => dispatch(setGameStatus(status)),
  stopGame: () => dispatch(stopGame()),
  loadStats: () => dispatch(loadStats()),
})

export default connect(mapStateToProps,mapDispatchToProps)(WSWrapper)
