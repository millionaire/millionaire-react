import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'


// Route's render() called when path is matched
// ...props are Route's props, don't touch 'em. they're: match, location, history, staticContext
const PrivateRoute = ({ component: Component, logged_in, ...rest }) => {
  return(
  <Route {...rest} render={props => {   
    // console.log('666 ZzzZ from PrivateRoute.Route', logged_in)
    switch (logged_in) {
      case undefined:
        return <h1>Checkig login status</h1>
      case false:
        console.log('false')
        return <Redirect to={{
          pathname: '/login',
          state: { from: props.location } // props here props of PrivateRoute. to=X, X=location object
        }}/>
      case true:
        return <Component {...props} />
      default:
        return
    }
  }} />)
}

const mapStateToProps = state => {
  return {
    logged_in: state.logged_in,
  }
}

export default connect(mapStateToProps)(PrivateRoute)
