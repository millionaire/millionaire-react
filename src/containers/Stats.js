import React 		 from 'react'
import { connect }   from 'react-redux'
import '../style/stats.css'
import loadStats from '../actions/loadStats'

class Stats extends React.Component {
  componentWillMount() {
    this.props.loadStats()
    // console.log('hello stats')
  }

  compare(a, b) {
    if (a.money > b.money) { return -1 }
    if (a.money < b.money) { return  1 }
    return 0
  }

  render() {
    if (this.props.stats) {
      return(
        <table className='stats'>
          <thead>
            <tr>
              <th>игрок</th>
              <th>правильные ответы</th>
              <th>деньги</th>
            </tr>
          </thead>
          <tbody>{
            this.props.stats.sort(this.compare).map(u =>
              <tr key={u.username} className={u.failed ? 'failed' : null}>
                <td>{u.username}</td>
                <td>{u.correctAnswers}</td>
                <td>{u.money}</td>
              </tr>
            )
          }</tbody>
        </table>
      )
    }
    else {
      return <div className='stats'>загрузка статистики</div>
    }

  }
}

const mapStateToProps = state => ({
  stats: state.stats,
  // isPreliminary: state.preliminary,
  // hostId: state.hostId,
  // activePlayerId: state.activePlayerId,
  // userId: state.userId
})

const mapDispatchToProps = dispatch => { 
  return {
    loadStats: () => dispatch(loadStats()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Stats)
