import React from 'react'
import { connect } from 'react-redux'
import { fetchQuestionHintCall,startHintCall } from '../actions/game_fetches'

const ContTimer = ({ fetchQuestionHintCall,startHintCall, used, started }) => (
    <li onClick={() => {
        if(!used) {
            (started ? startHintCall() : fetchQuestionHintCall())
        }
    }}
        className={!used ? ( started ? 'selectedCallfriend' : 'callfriend') : 'usedCallfriend'}></li>
)

const mapStateToProps = state => ({
  used: state.usedTimer1,
  started: state.startedTimer
})

const mapDispatchToProps = dispatch => ({
  startHintCall: () => dispatch(startHintCall()),
  fetchQuestionHintCall: () => dispatch(fetchQuestionHintCall())
})

export default connect(mapStateToProps, mapDispatchToProps)(ContTimer)
