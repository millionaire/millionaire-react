import React from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { loadRoomList, fetchRoom } from '../actions/game_fetches'
import newRoom from '../actions/newRoom'
import joinRoom from '../actions/joinRoom'
// import redirectToRoom from '../actions/joinRoom'
import loadRoomListData from '../actions/loadRoomListData'
import '../style/roomList.css'
import Button from 'material-ui/Button'

class RoomList extends React.Component {
  componentWillMount() {
    this.props.loadRoomListData()
  }

  getHostName(room) {
    for(var i = 0; i<=room.users.length-1; i++) {
      if (room.users[i].id === room.host) {
        return room.users[i].name
      }
    }
    return '-'
  }

  render() {
    // console.log('RoomList component before render()')
    if (this.props.roomToRedirect) {
      return <Redirect to={`/room/${this.props.roomToRedirect}`}/>
    }
    else {
      return (
        <div className='roomList'>
          <table>
            <caption>
              <h1>Выберите комнату</h1>
              <br/>
              <Button variant="raised" color="secondary" onClick={this.props.newRoom}>Создать комнату</Button>
            </caption>
            <thead>
              <tr>
                <th>Номер</th>
                <th>Ведущий</th>
                <th>Кол-во игроков</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{
              this.props.roomList !== undefined && this.props.roomList.slice(0).reverse().map(room =>
                <tr key={room.id} className={room.active ? 'gray_row': room.id === this.props.joinedRoomId ? 'selected_row':'normal_row'}>
                  <td>{room.id}</td>
                  <td>{this.getHostName(room)}</td>
                  <td>{room.users.length}</td>
                  {
                    room.active ? 
                    <td><div>Игра идет</div></td>:
                    room.id === this.props.joinedRoomId ?
                    <td><Link to={`/room/${this.props.joinedRoomId}`}>Вернуться</Link></td>:
                    <td className='green_btn'><Link to={`/room/${room.id}`} onClick={() => this.props.joinRoom(room.id)}>Зайти</Link></td>
                  }
                </tr>
              )
            }
            </tbody>
          </table>
        </div>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    roomList: state.roomList,
    joinedRoomId: state.joinedRoomId,
    roomToRedirect: state.roomToRedirect,
    sync: state.sync,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadRoomList: () => dispatch(loadRoomList()),
    joinRoom: id => dispatch(joinRoom(id)),
    fetchRoom: () => dispatch(fetchRoom()),    
    newRoom: () => dispatch(newRoom()), 
    loadRoomListData: () => dispatch(loadRoomListData()),   
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomList)
