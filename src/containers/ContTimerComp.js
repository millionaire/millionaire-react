import { connect } from 'react-redux'
import React from 'react';
import {hintCallFinished} from '../actions/game_fetches'
import '../style/timer.css'

class Timer extends React.Component{
    state = {
        elapsed: this.props.start
    }

    componentDidMount() {
        this.timer = setInterval(this.tick, 1000);
    }

    componentWillUnmount() {
         clearInterval(this.timer);
    }

    tick = () => {
        this.setState({elapsed: this.props.used ? ((this.state.elapsed >= 0) ? this.state.elapsed-1 : -1) : 30});
        if (this.state.elapsed < 0) {
            this.props.hintCallFinished();
        }
    }

    render() {
        var elapsed =  this.state.elapsed ;
         return (
            <div className="timer" style={((this.state.elapsed < 0) || (!this.props.used)) ? {display: 'none'} : {}}>
                <span>{elapsed}</span>
            </div>
         );
    }
}

const mapStateToProps = state => ({
  used: state.usedTimer,
  start: 30
})

const mapDispatchToProps = dispatch => {
  return {
    hintCallFinished: () => dispatch(hintCallFinished()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Timer)
