import React 		 from 'react';
import { connect }   from 'react-redux';

const StatusBar = ({isActive, logged_in}) => {
	if (isActive || !logged_in) // active player or observer, todo: maybe use if (!(userId === hostId)) // if not host
		return (<div className = "wall"></div>)
	else
		return null
}

const mapStateToProps = state => ({
	isActive: (state.userId === state.activePlayerId),
	logged_in: state.logged_in,
})

export default connect(mapStateToProps)(StatusBar)
