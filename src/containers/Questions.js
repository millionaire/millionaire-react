import React         from 'react'
import { connect } from 'react-redux'
import { fetchAnswer } from '../actions/game_fetches'
// import QuestionList from '../components/QuestionList'
import Diamond from '../components/Diamond'
import DiamondQuestion from '../components/DiamondQuestion'
import DiamondQuestionSmall from '../components/DiamondQuestionSmall'
import '../style/question.css'
import { toArrayAnswers } from '../actions/game_fetches'
import { fetchPreliminaryAnswer } from '../actions/game_fetches'
import MediaQuery from 'react-responsive'


const Questions = ({ prelimCorrectArray,threebeepsplayed,hostId, userId, activePlayerId, rezArray, index, fetchPreliminaryAnswer, answerArray, preliminary, used50501, question2, wrong5050Ids, question, show_result, clicked_id, result, fetchAnswer, toArrayAnswers, logged_in }) => {
  let letter = String.fromCharCode('A'.charCodeAt()-1);
  let correctfill = '#2E9E32'
  let wrongfill = '#e48200'
  let isClicked = function(answer) {
      for (var i = 0; i<=answerArray.length-1; i++) {
        if (answer.id === answerArray[i].answerId)
          return true;
      }
      return false;
    }
  let getfill = function(answer) {
    if (show_result && preliminary)
      return isClicked(answer) ? wrongfill: ''
    else
      return  show_result ? (answer.id === clicked_id ||  answer.id === result ? (answer.id === result ? correctfill:wrongfill):''):''
  }

  let answerOnClick =  function(answer) {
    if (answerActive(answer) && !isClicked(answer) && logged_in) {
      if (preliminary) {
        toArrayAnswers(answer.id);
        if (index === 3) {
          fetchPreliminaryAnswer();
        }
      }
      else {
        if(userId === activePlayerId)
          return;
        fetchAnswer(answer.id);
      }
    }
  }

  let answerActive = function (answer) {
    if (preliminary && !threebeepsplayed) {
      return false;
    }
    else {
    return ((answer.wrong5050 === false) &&
          !(answer.id===wrong5050Ids[0] || answer.id===wrong5050Ids[1]));
    }

  }

  const makeDiamond = answer => {
    return(
      <Diamond
        userId={userId}
        isActive={answerActive(answer)}
        key={ answer.id }
        text={ answer.text }
        letter = {letter = String.fromCharCode(letter.charCodeAt()+1)}
        onClick={() => answerOnClick(answer)}
        fill={getfill(answer)}
      />
    )
  }

  return (
    <div className='question_pannel'>

    <MediaQuery minWidth={600}>{(matches) => {
      if (matches) { return(
        <div>
          <DiamondQuestion text={question.text} />
       </div>
       )
      }
      else { return <DiamondQuestionSmall text={question.text} />}
    }}</MediaQuery>




      <div className='answers'>
        <div className='row'>
          <div className='answer_diamond'>{makeDiamond(question.answerDTOs[0])}</div>
          <div className='answer_diamond'>{makeDiamond(question.answerDTOs[1])}</div>
        </div>
        <div className='row'>
          <div className='answer_diamond'>{makeDiamond(question.answerDTOs[2])}</div>
          <div className='answer_diamond'>{makeDiamond(question.answerDTOs[3])}</div>
        </div>
      </div>
    </div>
  )
  // }

}


const mapStateToProps = state => ({
  question: state.question,
  wrong5050Ids: state.wrong5050Ids,
  question2: state.question2,
  show_result: state.show_result,
  clicked_id: state.clicked_id,
  result: state.result,
  loading: state.loading,
  used50501: state.used50501,
  preliminary: state.preliminary,
  answerArray: state.answerArray,
  index: state.index,
  rezArray: state.rezArray,
  activePlayerId: state.activePlayerId,
  userId: state.userId,
  hostId: state.hostId,
  prelimCorrectArray: state.prelimCorrectArray,
  threebeepsplayed: state.threebeepsplayed,
  logged_in: state.logged_in,
})

const mapDispatchToProps = dispatch => ({
  fetchAnswer: (clicked_id)  => dispatch(fetchAnswer(clicked_id)),
  toArrayAnswers: (clicked_id)  => dispatch(toArrayAnswers(clicked_id)),
  fetchPreliminaryAnswer: () => dispatch(fetchPreliminaryAnswer()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Questions)
