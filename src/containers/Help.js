import React      from 'react';
import { connect } from 'react-redux'
import { loadVoiceCommands } from '../actions/voiceCommands'
import '../style/help.css'


class Help extends React.Component {

  componentWillMount() {
    this.props.loadVoiceCommands()
  }

  makeCommandList(command) {
    return(
      <div className='commandPhrasesBlock'>
        <h3>{command}</h3>
        <ul>
          {this.props.voiceCommands[command].map(phrase => <li>{phrase}</li>)}
        </ul>
      </div>
    )
  }

  render() {
    if (!Object.keys(this.props.voiceCommands).length) {
      return<h1>Загрузка...</h1>
    }
    else {
      return(
        <div className='helpBlock'>
          <h1>Об игре</h1>
          <p>
            Игра представляет собой викторину. Упор сделан на оффлайн - компания людей собирается вместе. Выбирается ведущий, он создает комнату.
            Ведущий во время игры будет выбирать ответы, названные игроком, активировать подсказки.
          </p>
          <p>
          После начала игры игроки на время отвечают на отборочный вопрос, один из них становится активным игроком и выходит к ведущему. Активный игрок говорит ответы на вопросы ведущему, остальные игроки отвечают на те же вопросы на телефоне. Если активный игрок ошибается, то активным становится другой игрок. Игра идёт до победы одного из игроков либо до вылета всех игроков.
          </p>
          <p>
          </p>
          <p>
            Менеджментом вопросов и аккаунтов пользователей занимается администратор.
          </p>
          <p>
            В игру можно попасть либо зарегистрировавшись и выбрав комнату либо перейдя по QR коду или сокращенной ссылке. Тогда временный пользователь со случайно сгенерированный ником перейдет сразу в нужную комнату.
          </p>
          <p>
            Есть роль администратора сайта. Можно создавать / редактировать / удалять вопросы, удалять / банить пользователей.
          </p>
          <p>
            Для удобства ведущего наше веб приложение также поддерживает распознавание определенных фраз и реагирует на них.
          </p>
          <h2>Список голосовых фраз</h2> {[
            'продолжить игру',
            '1 ответ',
            '2 ответ',
            '3 ответ',
            '4 ответ',
            'подсказка 50 на 50',
            'подсказка помощь зала',
            'подсказка звонок другу',
          ].map(command => this.makeCommandList(command))}
        </div>
      )
    }
  }
}
// const Help = ({voiceCommands}) => {

// }


const mapStateToProps = state => {
  return {
    voiceCommands: state.voiceCommands,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadVoiceCommands: () => dispatch(loadVoiceCommands()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Help)
