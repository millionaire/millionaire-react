import React from 'react'
import { connect } from 'react-redux'
import { resetActSound } from '../actions/game_fetches'
import Sounds from '../components/Sounds'


const ActionSound = ({url,resetActSound}) => { //fetchQuestion,fetchNext
	return url != null && <Sounds url = {url} onFinishedPlaying={resetActSound} /> 
}

const mapStateToProps = state => { 
    return {
		url: state.actSound,
        //fetchNext: state.show_result && !state.showNoMorePlayerAlert && !state.showPlayerAlert && !state.showGameOverWonAlert
	} 
}

const mapDispatchToProps = dispatch => { 
  return {
    //fetchQuestion: () => dispatch(fetchQuestion()),
    resetActSound: () => dispatch(resetActSound()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionSound)
