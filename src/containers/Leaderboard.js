import React      from 'react';
import { connect } from 'react-redux'
import loadLeaderBoard from '../actions/loadLeaderBoard'
import '../style/leaderBoard.css'

class Leaderboard extends React.Component {

  componentWillMount() {
    this.props.loadLeaderBoard()
  }

  compare(a, b) {
    if (a.money > b.money) { return -1 }
    if (a.money < b.money) { return  1 }
    return 0
  }

  render() {
      return(
        <div className='leaderBoard'>
          <h1>Leaderboard</h1>
            <table>
            <thead>
              <tr>
                <th>номер</th>
                <th>игрок</th>
                <th>деньги</th>
              </tr>
            </thead>
            <tbody>{
              this.props.leaderBoard.sort(this.compare).map((user, index) =>
                <tr key={user.id} >
                  <td>{index + 1}</td>
                  <td>{user.name}</td>
                  <td>{user.money}</td>
                </tr>
              )
            }
            </tbody>
            </table>
        </div>
      )
  }
}


const mapStateToProps = state => {
  return {
    leaderBoard: state.leaderBoard,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadLeaderBoard: () => dispatch(loadLeaderBoard()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Leaderboard)
