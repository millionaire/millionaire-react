import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { loadUserList, deleteUser, updateUser, updatedUser} from '../../actions/admin'
import { redirectToReferrerFalse } from '../../actions/auth'
import '../../style/roomList.css'
import { UserRow } from './UserRow'
import  RingLoader from '../../components/RingLoader'


class UserList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeRow: null
    }
  }

  componentDidMount() {
    this.props.loadUserList()
  }

  render() {
    let setActiveRow = rowId => {
      this.setState({
        activeRow: rowId,
      });
    }

    const { from } = this.props.location.state || { from: { pathname: '/' } }
    if (this.props.login_redirectToReferrer) {
      this.props.redirectToReferrerFalse()
      return <Redirect to={ from } />
    }
    else
      if (this.props.userList && this.props.userList.length === 0)
        return <RingLoader color={'#eee'} />
      else
        return (
          <div className='adminTable'>
            <table>
              <caption>Пользователи</caption>
              <thead>
                <tr>
                  <th>Имя</th>
                  <th>Админ</th>
                  <th>Бан</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>{
                this.props.userList.map(user =>
                  <UserRow
                    user={user}
                    key={user.id}
                    deleteUser = {this.props.deleteUser}
                    updateUser = {this.props.updateUser}
                    setActiveRow = {setActiveRow}
                    activeRow = {this.state.activeRow}
                  />
                )
              }
              </tbody>
            </table>
          </div>
        )
  }
}


const mapStateToProps = state => {
  console.log(state.userList)
  return {
    userList: state.userList,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadUserList: () => dispatch(loadUserList()),
    deleteUser:  id => dispatch(deleteUser(id)),
    updateUser: (id, user) => dispatch(updateUser(id, user)),
    updatedUser: user => dispatch(updatedUser(user)),
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
