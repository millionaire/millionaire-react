import React from 'react';
import { loadVoiceCommands, updateVoiceCommands } from '../../actions/voiceCommands'
import { connect } from 'react-redux'
import '../../style/voiceCommandsEditor.css'

class VoiceCommands extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.props.voiceCommands
  }

  componentWillMount() {
    this.props.loadVoiceCommands() // need rerender when got response from server
  }

  makeCommandFieldSet(command) {
    let phrases = ''
    this.props.voiceCommands[command].forEach(p =>
      phrases += p + '\n'
    )

    this.props.voiceCommands[command].map(p => p+ '\n')
    return(
      <fieldset className='command' key={command}>
        <legend>{command}</legend>
        <textarea
          style={{fontFamily:'Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace'}}
          rows='8'
          cols='50'
          defaultValue = {phrases}
          onChange={ event => {
            this.setState({
              [command]: [...new Set(
                event.target.value.trim().toLowerCase().split('\n').filter(p => p !== '')
              )]
            })
          }}
        >
        </textarea>
      </fieldset>
    )
  }

  render() {
    console.log(this.state)
    if (!Object.keys(this.props.voiceCommands).length) {
      return<h1>Загрузка...</h1>
    }
    else {
      return (
      <div className='voiceCommandsEditor'>
        <h1>Редактор голосовых команд</h1>
        <div style={{color:'white', fontSize: '0.8em'}}>
          <p>Введите фразы на которые должны срабатывать комманды. Каждая фраза с новой строки.</p>
        </div>
        
        {
          [
            'продолжить игру',
            '1 ответ',
            '2 ответ',
            '3 ответ',
            '4 ответ',
            'подсказка 50 на 50',
            'подсказка помощь зала',
            'подсказка звонок другу',
          ].map(command => this.makeCommandFieldSet(command))
        }

        <button
          className='white_button voiceCommands_save_button'
          onClick={ event => {
            this.props.updateVoiceCommands(this.state)
          }}>
        Save
        </button>
      </div>
    )
    }
  }
}

const mapStateToProps = state => {
  return {
    voiceCommands: state.voiceCommands,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadVoiceCommands: () => dispatch(loadVoiceCommands()),
    updateVoiceCommands: voiceCommands => dispatch(updateVoiceCommands(voiceCommands)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VoiceCommands)
