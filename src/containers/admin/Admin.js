import React from 'react';
import { Link } from 'react-router-dom';
import '../../style/admin.css'

const Admin = () => {
	return (
		<div className='adminRoom'>
			<Link to='/admin/questions'><button className='green_button'>Управление вопросами</button></Link>
            <Link to='/admin/users'><button className='green_button'>Управление пользователями</button></Link>
			<Link to='/admin/voice_commands'><button className='green_button'>Голосовые команды</button></Link>
		</div>
	)
}

export default Admin
