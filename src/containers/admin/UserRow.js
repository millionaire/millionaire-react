import React from 'react'
import '../../style/roomList.css'

//TODO: move to components

export class UserRow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.user.id,
      name: this.props.user.name,
      clicked: false,
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleActivateRow = this.handleActivateRow.bind(this);
    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    this.handleBanUser = this.handleBanUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked ? 1 : 2: target.value;
    const name = target.name;

    if(name === "userRoleId") {
      let newuser = this.props.user
      newuser.userRoleId = value
      this.handleUpdateUser(newuser)
    }
    else {
      this.setState({
        [name]: value
      })
    }
  }
  handleActivateRow() {
    this.setState({
      clicked: !this.state.clicked,
    });
    if (!this.state.clicked) {
      this.props.setActiveRow(this.props.user.id)
    }
  }
  handleUpdateUser(user) {
    console.log('handleUpdateUser:', this.props.user.userRoleId)
    this.setState({
      clicked: !this.state.clicked,
    },
      this.props.updateUser(this.state.id, user)
    )
  }

  handleBanUser() {
    let newuser = this.props.user
    newuser.userRoleId = (newuser.userRoleId === 3 ? 2 : 3)
    this.handleUpdateUser(newuser)
  }

  handleSubmit(event) {
    let newuser = this.props.user
    newuser.name = this.state.name
    this.handleUpdateUser(newuser)
    event.preventDefault();
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      userRoleId: nextProps.user.userRoleId,
      name: nextProps.user.name,
      clicked: nextProps.activeRow != null && nextProps.activeRow === nextProps.user.id
    }
  }
  render() {
      return (
        <tr className={'smpleRow'}
            onClick = {!this.state.clicked ? this.handleActivateRow : () => {console.log('asdasdasdasd:')}}>
          <td>
          {!this.state.clicked ? <div>{this.props.user.name}</div> :
            <form onSubmit={this.handleSubmit}>
              <input
                  type="text"
                  placeholder="name"
                  name="name"
                  value={this.state.name}
                  onChange={this.handleInputChange}
                />
            </form>
          }
          </td>
          <td>
            <input
              type="checkbox"
              name="userRoleId"
              checked={this.props.user.userRoleId === 1 ? "checked": ''}
              onChange={this.handleInputChange}
            />
          </td>
          <td>
            <button
              onClick = {this.handleBanUser}
              className='white_button'
            >
              {this.props.user.userRoleId === 3 ? 'разбанить' : 'бан'}
            </button>
          </td>
          <td>
            <button
              onClick = {() => this.props.deleteUser(this.state.id)}
              className='red_button_header'
            >
              удалить
            </button>
          </td>
        </tr>
      )
  }
}
