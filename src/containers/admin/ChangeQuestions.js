import React from 'react'
import '../../style/login_form.css'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { redirectToReferrerFalse } from '../../actions/auth'


import { Link } from 'react-router-dom';

const ChangeQuestions = props => {
    const { from } = props.location.state || { from: { pathname: '/' } }
    if (props.login_redirectToReferrer) {
      props.redirectToReferrerFalse()
      return <Redirect to={ from } /> 
    } 
    else 
    return (
		<div>
			<Link to='/admin/newquestion'><button className='green_button'>Добавить вопрос</button></Link>
			<Link to='/admin/deletequestion'><button className='green_button'>Удалить вопрос</button></Link>
		</div>
	)
}
const mapStateToProps = state => {
  return {
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChangeQuestions)


