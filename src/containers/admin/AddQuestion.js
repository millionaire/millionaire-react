import React from 'react'
import { connect } from 'react-redux'
import { saveQuestion, getCategory } from '../../actions/admin'
import '../../style/adminAddQuestion.css'
import { Link } from 'react-router-dom'







class AddQuestion extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      category: '0',
      question: '',
      answer1: '',
      answer2: '',
      answer3: '',
      answer4: '',
      answerArray: undefined,
      right: undefined,
    }
  }

  componentDidMount() {
    this.props.getCategory()
    console.log(this.state)
  }

  render() {
    return (
        <div>
        <Link to='/admin/questions'><button className='green_button'>Назад</button></Link>
          
       

          <form className="addQuestionForm">
            {console.log(this.state)}
            <select onChange={(event) => { this.setState({category: event.target.value}) }} >
              {this.props.categoryesList1.map(category => 
                <option key={category.ord} value={category.ord} >{category.ord===-1 ? 'отборочный' : category.ord+1} вопрос</option>
              )}
            </select>

            <div className='answerRow'>
              <textarea
                placeholder="вопрос"
                cols="40"
                rows="4"
                onChange={(event) => { this.setState({question: event.target.value}) }}
              />
              <span>правильный<br/>ответ:</span>
            </div>


            <div className='answerRow'>
              <textarea
                placeholder="ответ A"
                cols="40"
                rows="4"
                onChange={(event) => { 
                  this.setState({answer1: event.target.value}) 
                }}
              />
              <input type="radio" name="isAnswerRight" onChange={event => { this.setState({right: 1}) }} />
            </div>

            <div className='answerRow'>
              <textarea
                placeholder="ответ B"
                cols="40"
                rows="4"
                onChange={(event) => { 
                  this.setState({answer2: event.target.value})
                }}
              />
              <input type="radio" name="isAnswerRight" onChange={event => { this.setState({right: 2}) }} />
            </div>

            <div className='answerRow'>
              <textarea
                placeholder="ответ C"
                cols="40"
                rows="4"
                onChange={(event) => {
                  this.setState({answer3: event.target.value})
                }}
              />
              <input type="radio" name="isAnswerRight" onChange={event => { this.setState({right: 3}) }} />
            </div>

            <div className='answerRow'>
              <textarea
                placeholder="ответ D"
                cols="40"
                rows="4"
                onChange={(event) => {
                  this.setState({answer4: event.target.value})
                }}
              />
              <input type="radio" name="isAnswerRight" onChange={event => { this.setState({right: 4}) }} />
            </div>

            <button className='white_button' onClick={(event) => {
              event.preventDefault();
              this.props.saveQuestion(
                this.state.category,
                this.state.question,
                this.state.answer1,
                this.state.answer2,
                this.state.answer3,
                this.state.answer4,
                this.state.right)
            }}>Сохранить</button>
          </form>
        </div>
        )
    }
  }


const mapStateToProps = state => { 
  return {
    categoryesList: state.categoryesList,
    categoryesList1: state.categoryesList1,
    login_fail_alert: state.login_fail_alert,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    saveQuestion: (category, question, answer1, answer2, answer3, answer4, right) => dispatch(saveQuestion(category, question, answer1, answer2, answer3, answer4, right)),
    getCategory: () => dispatch(getCategory())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestion)

