import React from 'react'
import { connect } from 'react-redux'
import ButtonPoll from '../components/ButtonPoll'
import { fetchQuestionHintPoll } from '../actions/game_fetches'

const ContPoll = ({ fetchQuestionHintPoll, question2, used }) => (
    <ButtonPoll fetchQuestionHintPoll={ fetchQuestionHintPoll } used = {used}/>
)

const mapStateToProps = state => ({
  question2: state.question2,
  used: state.usedPoll,
})

const mapDispatchToProps = dispatch => ({ 
  fetchQuestionHintPoll: () => dispatch(fetchQuestionHintPoll()) 
})

export default connect(mapStateToProps, mapDispatchToProps)(ContPoll)
