import React from 'react'
import { connect } from 'react-redux'
import Score from '../components/Score'


const FetchScore = ({active}) => {
	return <Score active = {active}/>
}

const mapStateToProps = state => {
    return {
		active: state.question.ord
	}
}
export default connect(mapStateToProps)(FetchScore)
