import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { try_log_in } from '../actions/auth'
import { redirectToReferrerFalse } from '../actions/auth'
import '../style/login_form.css'
import Button from 'material-ui/Button'

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
    }
  }
  
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    console.log('Login.render() from: ',from, 'props.login_redirectToReferrer', this.props.login_redirectToReferrer)
    if (this.props.login_redirectToReferrer) {
      this.props.redirectToReferrerFalse()
      return <Redirect to={ from } /> 
    }
    else {
      let form_status_class
      let error_message

      if (this.props.login_fail_alert) {
        form_status_class = 'incorrect_form'
        error_message = this.props.login_fail_alert
      }
      else {
        form_status_class = 'normal_form'
      }

      return (
        <div className={ form_status_class }>
          
          { error_message }

          <form>
            <input  
              type="text"
              placeholder="имя"
              onChange={(event) => { this.setState({username: event.target.value}) }}
            />
            <br/>
            <input  
              type="password" 
              placeholder="пароль" 
              onChange={(event) => { this.setState({password: event.target.value}) }}
            />
            <br/>
            <Button variant="raised" onClick={(event) => {
              event.preventDefault();
              this.props.try_log_in(this.state.username, this.state.password)
            }}>
            Войти
            </Button>
          </form>
        </div>
        )
    }
  }
}

const mapStateToProps = state => { 
  return {
    login_fail_alert: state.login_fail_alert,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    try_log_in: (username, password) => dispatch(try_log_in(username, password)),
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
