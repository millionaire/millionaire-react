import React from 'react'
import Button from 'material-ui/Button'

const LogOutButton = ({ log_out, history }) => {
  return ( 
    <Button
      size="small"
      onClick={() => {
      log_out()
      }}>
    Выйти
    </Button>
  )
}


export default LogOutButton
