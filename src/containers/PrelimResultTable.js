import React from 'react';
import { connect } from 'react-redux';
import { continuePrelim } from '../actions/game_fetches'
import '../style/prelimresulttable.css'
import Diamond from '../components/Diamond'
import Button from 'material-ui/Button'

const PrelimResultTable = ({loading,preliminary,prelimCorrectArray,answerDTOs,prelimPlayers,continuePrelim,userId,hostId}) => {
    let delay = -0.2;
    let between = 3.1;
    let orderf = (ansid) => {
        let found = prelimCorrectArray.find(x => x.answerId === ansid)
        return found.ord
    }
    let getDelay = (at) => {
        return delay + between * at;
    }
    let getMaxDelay = () => {
      return between * 5;
    }
    let letter = String.fromCharCode('A'.charCodeAt()-1);
        return(
          <div>

          <div className='controls'>
            <div className='answers answertable prelimPlayerTable' style={{'animationDelay':(getMaxDelay() - 0.3)+'s',}}>
              {
                prelimPlayers != null && prelimPlayers.map(player =>
                  <Diamond
                    isActive={true}
                    key={ player.id }
                    text={ player.name + ' (' + player.answerTime+ ')' }
                    className={'answerwithuser '}
                    blinker={player.winner}
                    // correctfill={player.correct}
                    fill={player.correct && '#2E9E32'}

                  />
                )
              }
            </div>
            <div className={'answers answertable prelimAnswerTable'} style={{'animationDelay':(getMaxDelay() - 0.4)+'s',}}>
                {
                    answerDTOs.map(ans => <Diamond
                        isActive={true}
                        key={ ans.id }
                        text={ ans.text }
                        className={'prelimAnswer'}
                        letter = {letter = String.fromCharCode(letter.charCodeAt()+1)}
                        style={{'order':orderf(ans.id),
                                'animationDelay':getDelay(orderf(ans.id))+'s',
                                  }}
                    />)
                }

            </div>

          </div>
          {
               (userId === hostId) && <Button variant="raised" color="secondary"onClick={continuePrelim}>начать игру</Button>
            }
          </div>

        )

}

const mapStateToProps = state => ({
  loading: state.loading,
  preliminary: state.preliminary,
  prelimCorrectArray: state.prelimCorrectArray,
  answerDTOs: state.question.answerDTOs,
  prelimPlayers: state.prelimPlayers,
  userId: state.userId,
  hostId: state.hostId,
})

const mapDispatchToProps = dispatch => {
  return {
    continuePrelim: () => dispatch(continuePrelim()),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PrelimResultTable);
