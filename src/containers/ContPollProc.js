import React from 'react'
import { connect } from 'react-redux'
import PollProcess from '../components/PollProcess'

const ContPollProc = ({ used }) => (
    <PollProcess used = {used} start = {16}/>
)

const mapStateToProps = state => ({
  used: state.usedPoll
})

export default connect(mapStateToProps)(ContPollProc)