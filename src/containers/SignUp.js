import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { signup } from '../actions/auth'
import '../style/login_form.css'
import Button from 'material-ui/Button'

class SignUp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      passwordConfirm: '',
    }
  }
  
  render() {
    const { from } = { from: { pathname: '/' } }
    if (this.props.login_redirectToReferrer) {
      return <Redirect to={ from } /> 
    }
    else {
      let form_status_class
      let error_message

      if (this.props.login_fail_alert) {
        form_status_class = 'incorrect_form'
        error_message = this.props.login_fail_alert
      }
      else {
        form_status_class = 'normal_form'
      }

      return (
        <div className={ form_status_class }>
          { error_message }
          
          <form>
            <input  
              type="text"
              placeholder="имя"
              onChange={(event) => { this.setState({username: event.target.value}) }}
            /><br/>

            <input  
              type="password" 
              placeholder="пароль" 
              onChange={(event) => { this.setState({password: event.target.value}) }}
            /><br/>

            <input  
              type="password" 
              placeholder="подтвердите пароль" 
              onChange={(event) => { this.setState({passwordConfirm: event.target.value}) }}
            /><br/>

            <Button variant="raised" onClick={(event) => {
              event.preventDefault();
              this.props.signup(this.state.username, this.state.password, this.state.passwordConfirm)
            }}>
              Зарегистрироваться
            </Button>
            </form>

        </div>
      )
    }
  }
}

const mapStateToProps = state => { 
  return {
    login_fail_alert: state.login_fail_alert,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    signup: (username, password, passwordConfirm) => dispatch(signup(username, password, passwordConfirm)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
// export default withStyles(styles
