import React from 'react'
import '../../style/waitGame.css'
import { connect } from 'react-redux'
import startGameServer from '../../actions/startGame'
import WSWrapper     from '../WSWrapper'
import loadRoomListData from '../../actions/loadRoomListData'
import enableInvite from '../../actions/enableInvite'
import { syncComplete } from '../../actions/sync'
import { fetchOneRoom } from '../../actions/game_fetches'
import Button from 'material-ui/Button'

class WaitGame extends React.Component {
  
  render() {
    return (
      <div className='waitGame'>
        <table>
          <caption>Комната {this.props.roomId}</caption>
            <thead>
              <tr>
                <th>Игроки</th>
            </tr>
          </thead>
          <tbody>{
            this.props.roomUserList.map(user =>
              <tr key={user.id}  style={user.id === this.props.userId ? {color: '#E91E63'} : {}}>
                <td>{user.name} {user.id === this.props.hostId ? '(ведущий)' : ''}</td>
              </tr>
              )
          }
          </tbody>
          </table>
          {this.props.userId === this.props.hostId ?
            <div className='waitGameAdminControlls'>
              <Button variant="raised" color="secondary" 
              onClick={() => { this.props.startGame(this.props.roomId)
              }}>
              начать игру
              </Button>

              <br/>

              {
                this.props.tempRoomUrl ? 
                <div className='inviteStuff'>
                  <img src={ 'https://chart.googleapis.com/chart?cht=qr&chs=547x547&chl=' + this.props.tempRoomUrl } alt='qr code for room invite' />
                  
                  <div className='inviteUrl'>
                    инвайт-ссылка:
                    <input className='' type='text' value={ this.props.tempRoomUrl }></input>
                  </div>
                </div>
                :
                  <Button variant="raised" color="primarylight" 
                  onClick={() => { this.props.enableInvite(this.props.roomId) }}>
                  Пригласить в игру
                  </Button>
              }
            </div>
          :
            <p>Wait until host start the game</p>
          }
        <WSWrapper roomId={ this.props.roomId } />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    roomUserList: state.roomUserList,
    joinedRoomId: state.joinedRoomId,
    activeRoom: state.activeRoom,
    userId: state.userId,
    hostId: state.hostId,
    sync: state.sync,
    tempRoomUrl: state.tempRoomUrl,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    startGame: roomId => dispatch(startGameServer(roomId)),
    loadRoomListData: () => dispatch(loadRoomListData()),
    syncComplete: () => dispatch(syncComplete()),
    fetchOneRoom: roomId =>dispatch(fetchOneRoom(roomId)),
    enableInvite: roomId => dispatch(enableInvite(roomId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WaitGame)
