import React         from 'react'
import { connect }   from 'react-redux'
import setGameStatus from '../../actions/setGameStatus'

class FailAlert extends React.Component {

  // componentDidMount() {
  //   setTimeout(() => {
  //     this.props.setGameStatus('failed')
  //   }, 3000)
  // }

  render() {
    const myStyle={
      color: 'white',
      backgroundColor: '#1B1D26',
    }

    let prize

    if (this.props.numberOfCorrectAnswers < 5) { prize = 0}
    else if (this.props.numberOfCorrectAnswers < 10) { prize = 1000 }
    else if (this.props.numberOfCorrectAnswers < 15) { prize = 32000 }
    return(
      <h2 style={myStyle}>Wrong answer. (or no answer) You prize is: ${prize}. You can continue game without prize.</h2>
    )
  }
}

const mapStateToProps = state => {
  return {
    gameStatus: state.gameStatus,
    numberOfCorrectAnswers: state.question.id - 1,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setGameStatus: status => dispatch(setGameStatus(status)),
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(FailAlert)


// else if (gameStatus === 'just_failed') {

//   return <h1>You failed. You can continue game w/o money</h1>
// }


