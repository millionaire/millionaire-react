import React         from 'react'
import { connect }   from 'react-redux'
import Question      from '../Questions'
import ActionSound   from '../ActionSound'
import Spinner       from '../Spinner'
import GameAlert     from '../GameAlert'
import Wall          from '../Wall'
import                    '../../style/game.css'
import PrelimResultTable from '../PrelimResultTable'
import BgSound       from '../BgSound'
import MediaQuery from 'react-responsive'
import { fetchQuestion } from '../../actions/game_fetches'

class Preliminary extends React.Component {

  componentWillMount() {
    this.props.fetchQuestion()
  }

  render(){
    return(
      <div className='game'>
        <div className='prelimchik'>
        <MediaQuery minWidth={600}>
          <div>
            <Wall />
            {this.props.userId !== this.props.hostId && <ActionSound /> && <BgSound />}        
          </div>
        </MediaQuery>
          {
            this.props.prelimCorrectArray !== null ? <PrelimResultTable /> : (
              this.props.userId === this.props.hostId && <div style={{display:'flex',justifyontent:'center'}}>
                <h1>Дождитесь окончания отборочного тура</h1>
              </div>
            )
          }
        </div>      
        <GameAlert />
        <Spinner />
        {this.props.userId !== this.props.hostId && this.props.prelimCorrectArray === null && <Question />}
      </div>
    )
  }

}

const mapStateToProps = state => {
  return {
    roomList: state.roomList,
    userId: state.userId,
    hostId: state.hostId,
    preliminary: state.preliminary,
    prelimCorrectArray: state.prelimCorrectArray,
    result: state.result,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    fetchQuestion: () => dispatch(fetchQuestion()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Preliminary)
