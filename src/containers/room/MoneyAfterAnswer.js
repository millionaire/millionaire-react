import React from 'react'
import { connect } from 'react-redux'
import loadStats from '../../actions/loadStats'

class MoneyAfterAnswer extends React.Component {
  componentWillMount() {
    this.props.loadStats()
    // console.log('hello stats from MoneyAfterAnswer')
  }


  render() {
    const myStyle = {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      // marginLeft: 'auto',
      // marginRight: 'auto',
      margin: 'auto',
      // paddingTop: '100px',
      // paddingBottom: '100px',
      backgroundColor: 'rgb(0, 0, 0, 0.2)',
    }

    const svgStyle = {
      position: 'absolute',
      top:' 50%',
      left: '50%',
      transform: 'translate(-50%,-50%)',
      width: '100%'
    }

    return(
      <div style={myStyle}>
      <svg viewBox='0 0 393 50' style = {svgStyle}>

      <linearGradient id="mygradient_1">
        <stop offset="0" stopColor="#0B4B9F" />
        <stop offset="1" stopColor="#C1E8E5" />
      </linearGradient>

      <defs>
      <radialGradient id="Gradient1" cy="60%" fx="90%" fy="70%" r="2">
          <stop offset="0%" stopColor="#48afc1" />
          <stop offset="10%" stopColor="#b4c63b" />
          <stop offset="20%" stopColor="#ef5b2b" />
          <stop offset="30%" stopColor="#503969" />
          <stop offset="40%" stopColor="#ab6294" />
          <stop offset="50%" stopColor="#1cb98f" />
          <stop offset="60%" stopColor="#48afc1" />
          <stop offset="70%" stopColor="#b4c63b" />
          <stop offset="80%" stopColor="#ef5b2b" />
          <stop offset="90%" stopColor="#503969" />
          <stop offset="100%" stopColor="#ab6294" />
        </radialGradient>
      </defs>

      <path
        stroke="url(#mygradient_1)"
        strokeWidth="1"
        fill="url(#Gradient1)"
        d="M1.5,25.86
        c6.388,4.04,21.2,23.368,30.858,23.71
        l328.492,0.082
        c9.66-0.342,24.262-19.964,30.65-24.005
        v-0.356c-6.388-4.04-21.199-23.367-30.858-23.709
        L32.149,1.5
        C22.49,1.842,7.888,21.464,1.5,25.505V25.86z"
      />

      <text
        textAnchor="middle"
        x="200" y="30"
        fontSize="30px"
        fill="white"
      >
      { '$' + this.props.money }
     </text>
    </svg>
    </div>
    )
  }
}
// const MoneyAfterAnswer = props => {
//   const money = [
//           '100',
//           '200',
//           '300',
//           '500',
//         '1 000',
//         '2 000',
//         '4 000',
//         '8 000',
//        '16 000',
//        '32 000',
//        '64 000',
//       '125 000',
//       '250 000',
//       '500 000',
//     '1 000 000',
//   ]




// }

const mapStateToProps = state => {
  let money

  if (state.last_failed_active_player_money) {
    money = state.last_failed_active_player_money
  }
  else {
    if (state.userId === state.hostId || state.userId === '') {
      let active_pl = state.roomUserList.filter(u => u.id === state.activePlayerId)
      if (active_pl.length > 0) {
        money = state.stats.filter(e => e.username === active_pl[0].name)[0].money
      }
    }
    else {
      money = state.stats.filter(e => e.username === state.username)[0].money
    }
  }
  return {
    joinedRoomId: state.joinedRoomId,
    currentQuestionNumber: state.question.ord,
    username: state.username,
    money: money,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadStats: () => dispatch(loadStats()),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(MoneyAfterAnswer)
