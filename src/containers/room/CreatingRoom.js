import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

const CreatingRoom = ({joinedRoomId}) => {
  if (joinedRoomId) {
    return <Redirect to={`/room/${joinedRoomId}`}/> 
  }
  else {
    return <h1>Creating room...</h1>
  }

} 

const mapStateToProps = state => { 
  return {
    joinedRoomId: state.joinedRoomId,
  }
}


export default connect(mapStateToProps)(CreatingRoom)
