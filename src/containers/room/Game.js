import React         from 'react'
import { connect }   from 'react-redux'
import Question      from '../Questions'
import ActionSound   from '../ActionSound'
import BgSound       from '../BgSound'
import ContHintPoll  from '../ContHintPoll'
import ContTimerComp from '../ContTimerComp'
import Spinner       from '../Spinner'
import SpeechRecognition from '../SpeechRecognition'
import GameAlert     from '../GameAlert'
import StatusBar     from '../StatusBar'
import Wall          from '../Wall'
import                    '../../style/game.css'
import Stats from '../Stats'
import MoneyAfterAnswer from './MoneyAfterAnswer'
import FailAlert from './FailAlert'
import MediaQuery from 'react-responsive'

const Game = props => {
  console.log("RRRRRR", props.result)
  return(
    <div className='game'>
        <MediaQuery minWidth={600}>
          <div className='controls'>
            {props.userId === props.hostId ?
              <div>
                <Stats />
                <SpeechRecognition />
              </div>
              :
              <div>
                <ActionSound />
                <BgSound />
              </div>
            }
            <Wall />
            <ContHintPoll />
            <ContTimerComp />
            <StatusBar />
          </div>
        </MediaQuery>
        { props.result != null &&  props.gameStatus === 'playing' && <MoneyAfterAnswer /> }
      { props.gameStatus === 'just_failed' && <FailAlert /> }
      <GameAlert />
      <Spinner />
      { props.showQuestion && <Question /> }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    roomList: state.roomList,
    userId: state.userId,
    hostId: state.hostId,
    preliminary: state.preliminary,
    prelimCorrectArray: state.prelimCorrectArray,
    result: state.result,
    gameStatus: state.gameStatus,
    showQuestion: state.showQuestion,
  }
}

export default connect(mapStateToProps)(Game)
