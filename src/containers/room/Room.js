import React from 'react'
import { connect } from 'react-redux'
import WaitGame from './WaitGame'
import Game from './Game'
import { fetchOneRoom, fetchQuestion } from '../../actions/game_fetches'
import { setJoinedRoomId } from '../../actions/game_fetches'
import Preliminary from './Preliminary';
import WSWrapper     from '../WSWrapper'


class Room extends React.Component {
  
  componentWillMount() {
    if (!this.props.logged_in) { // fake join room (room marks as joined only in frontend)
      this.props.setJoinedRoomId(this.props.match.params.room_id)
    }
    this.props.fetchOneRoom(this.props.match.params.room_id);
    if (this.props.question.text === "") {
          this.props.fetchQuestion()
    }
  }

  render() {
    if (this.props.roomUserList != null && this.props.roomUserList.length > 0) {
      if (this.props.gameStarted) {
        return(
          <div>
            <WSWrapper roomId={this.props.match.params.room_id} />  
            {this.props.preliminary ? <Preliminary /> : <Game />}
          </div>
        )
      }
      else {
        return <WaitGame roomId={ this.props.match.params.room_id } />
      }
    }
    else {
      return <h1>Загрузка...</h1>
    }
  }
}

const mapStateToProps = state => {
  return {
    joinedRoomId: state.joinedRoomId,
    gameStarted: state.gameStarted,
    roomUserList: state.roomUserList,
    question: state.question,
    logged_in: state.logged_in,
    preliminary: state.preliminary,    
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    fetchOneRoom: roomId => dispatch(fetchOneRoom(roomId)),
    setJoinedRoomId: roomId => dispatch(setJoinedRoomId(roomId)),
    fetchQuestion: () => dispatch(fetchQuestion()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Room)
