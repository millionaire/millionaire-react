import { loadRoomList } from './game_fetches'
import checkJoinedRoom  from './checkJoinedRoom'
import { syncStart, syncComplete } from './sync'

const loadRoomListData = () => {
  return dispatch => Promise.all([
    dispatch(syncStart()),
    dispatch(loadRoomList()),
    dispatch(checkJoinedRoom()),
  ]).then(() => dispatch(syncComplete()));
}

export default loadRoomListData
