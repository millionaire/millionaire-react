const redirectToRoom = roomId => ({
  type: 'REDIRECT_TO_ROOM',
  roomId,
})
export default redirectToRoom
