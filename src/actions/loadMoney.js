

import { rest_api_host } from '../index'

export const setMoney = money => ({
  type: 'SET_MONEY',
  money
})

const loadMoney = userId => {
  return dispatch => {
    return fetch(rest_api_host + '/api/users/' + userId)
      .then(response => {
        if (response.ok) { return response.json() }
      })
      .then(userInfo => {
        dispatch(setMoney(userInfo.money))
      })
  }
}

export default loadMoney
