import { rest_api_host } from '../index'
import { try_log_in }    from '../actions/auth'
import { setJoinedRoomId } from './game_fetches'

const useInviteLink = (roomId, inviteToken) => {
  return dispatch => {
    return fetch(rest_api_host + `/api/rooms/invite/${roomId}/${inviteToken}`)
      .then(response => {
        if (response.ok) { return response.json() }
        else { throw new Error('Room is not invitable')}
      })
      .then(tempUser => { 
        console.log(tempUser)
          dispatch(try_log_in(tempUser.name, tempUser.password))
          dispatch(setJoinedRoomId(roomId))
      })
      .catch((error) => {
        console.log(error)
      });
  }
}

export default useInviteLink
