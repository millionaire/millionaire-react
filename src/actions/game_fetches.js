import { rest_api_host } from '../index'
import { check_token } from './auth'
import { setInviteUrl } from './enableInvite'
import loadStats from './loadStats'

export const threeBeepsPlayed=()=> ({
  type: 'THREE_BEEPS_PLAYED',
})

const playLetsPlay = (ord) => ({
  type: 'PLAY_LETSPLAY',
  ord,
})

export const resetActSound = () => ({
  type: 'RESET_ACT_SOUND',
})

export const requestPosts=()=> ({
  type: 'REQUEST_POSTS',
})


export const playFinalAnswer = () => ({
  type: 'PLAY_FINALANSWER',
})

export const newPlayerAlert=()=> ({
  type: 'NEW_PLAYER_ALERT',
})

export const hideNewPlayerAlert=()=> ({
  type: 'HIDE_NEW_PLAYER_ALERT',
})

export const noMorePlayersAlert = () => ({
  type: 'NO_MORE_PLAYERS_ALERT',
})

export const hideNoMorePlayersAlert=()=> ({
  type: 'HIDE_NO_MORE_PLAYERS_ALERT',
})

export const resetGameAlerts=()=> ({
  type: 'RESET_GAME_ALERTS',
})

export const gameOverWonAlert=()=> ({
  type: 'GAME_OVER_WON_ALERT',
})

const receivePosts = (question) => {
  console.log('ПРИШЕЛ НОВЫЙ ВОПРОС', question)
  return({
  type: 'RECEIVE_POSTS',
  question
})}

export const receivePostsHint5050 = (wrongIds) => ({
  type: 'RECEIVE_POSTS_HINT_5050',
  wrongIds
})

export const receivePostsHintPoll = (question) => ({
  type: 'RECEIVE_POSTS_HINT_POLL',
  question
})

export const hintPollStarted = () => ({
  type: 'HINT_POLL_STARTED'
})

export const hintCallStarted = () => ({
  type: 'HINT_CALL_STARTED'
})

export const hintCallFinished = () => ({
  type: 'HINT_CALL_FINISHED'
})

export const receivePostsHintCall = () => ({
  type: 'RECEIVE_POSTS_HINT_CALL'
})

const createRoom = room => ({
  type: 'CREATE_ROOM',
  room
})

export const getOneRoom = room => ({
  type: 'GET_ONE_ROOM',
  room
})


export const toArrayAnswers = clicked_id => ({
  type: 'TO_ARRAY',
  clicked_id
})

export const fetchRoom = () => {
  return dispatch => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = { method: 'post', headers: { 'access_token': token }}
      dispatch(requestPosts())

      return fetch(rest_api_host + '/api/rooms', opts)
        .then(response => {
          if (response.ok) {
            return response.json()
          }
          else {
            dispatch(check_token())
          }
        })
        .then(room => dispatch(createRoom(room)))
    }
  }
}
//
export const fetchOneRoom = roomId => {
  roomId = parseInt(roomId, 10)
  return (dispatch, getState) => {
      return fetch(rest_api_host + '/api/rooms/' + roomId)
        .then(response => {
          if (response.ok) { return response.json() }
        })
        .then(room => {
          dispatch(getOneRoom(room))
          if (room.users.filter(r => r.id === getState().userId).length > 0) {
            dispatch(setJoinedRoomId(roomId))
          }
          if (room.inviteUrl) {
            dispatch(setInviteUrl(room.inviteUrl.slice(8)))
          }
          else {
            dispatch(setInviteUrl(undefined))
          }
        })
  }
}

export const fetchQuestion = () => {
  return (dispatch, getState) => {
      //dispatch(requestPosts())
      const {joinedRoomId,show_result,question} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/question')
        .then(response => {
          if (response.ok) { return response.json() }
          else {
            dispatch(check_token())
          }
        })
        .then(q => {
          if (q.ord >= 5 && show_result && q.ord - 1 === question.ord) {
            dispatch(playLetsPlay(q.ord))
          }
          else {
            dispatch(receivePosts(q))
          }
        })
        .then(() => {
          const {preliminary,threebeepsplayed,gameStarted} = getState()
          if (preliminary && !threebeepsplayed && gameStarted) {
            setTimeout(() => dispatch(threeBeepsPlayed()), 2500)
          }

        })
        .then(() => dispatch(loadStats()))
  }
}

export const skipLetsplay = () => {
  return (dispatch, getState) => {
      //dispatch(requestPosts())
      const {joinedRoomId} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/question')
        .then(response => {
          if (response.ok) { return response.json() }
          else {
            dispatch(check_token())
          }
        })
        .then(question => {
            dispatch(receivePosts(question))
        })
  }
}


export const continuePrelim = () =>{
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({ 'continuePrelim': true })
      }
      const {joinedRoomId} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId, opts)
    }
  }
}

export const continueGame = (delay) =>{
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      return new Promise((resolve, reject) => {

        const currentOrd = getState().question.ord;
        setTimeout(() => {
          const newOrd = getState().question.ord;
          const show_result = getState().show_result;
          if (show_result && currentOrd === newOrd) {
            resolve();
          }
          else {
            reject()
          }
        }, delay)

      }).then(() => {

          const {joinedRoomId,result,letsplayplayed} = getState();
            //showPlayerAlert,showNoMorePlayerAlert,showGameOverWonAlert,
          // let no_pop_ups = !(showPlayerAlert || showNoMorePlayerAlert || showGameOverWonAlert)
          let reqbody
          if( result === null ){
            reqbody = JSON.stringify({ 'finalAnswer': true })
          }
          else if (letsplayplayed === true) {
            reqbody = JSON.stringify({ 'skipLetsplay': true })
          }
          else {
            reqbody = JSON.stringify({ 'continueGame': true })
          }
          let opts = {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json',
              'access_token': token,
            },
            body: reqbody
          }
          return fetch(rest_api_host + '/api/rooms/' + joinedRoomId, opts)

      }).catch(()=>{})
    }
  }
}

export const anotherPlayer = () =>{
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({ 'anotherPlayer': true })
      }
      const {joinedRoomId} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId, opts)
    }
  }
}

export const fetchQuestionHint5050 = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = { headers: { 'access_token': token }}
      dispatch(requestPosts())
      const {joinedRoomId} = getState()

      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/question/hint/5050', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(question => dispatch(receivePostsHint(question)))
    }
  }
}

export const fetchQuestionHintPoll = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = { headers: { 'access_token': token }}
      dispatch(requestPosts())
      const {joinedRoomId} = getState()

      return fetch(rest_api_host + '/api/rooms/' +  joinedRoomId + '/question/hint/poll', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(question => dispatch(receivePostsHintPoll(question)))
    }
  }
}

export const startHintCall = () =>{
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({ 'startHintCall': true })
      }
      const {joinedRoomId} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId, opts)
    }
  }
}

export const fetchQuestionHintCall = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = { headers: { 'access_token': token }}
      const {joinedRoomId} = getState()

      return fetch(rest_api_host + '/api/rooms/' +  joinedRoomId + '/question/hint/call', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(question => dispatch(receivePostsHintPoll(question)))
    }
  }
}

export const preliminaryAnswerClicked = (answerArray, correctAnswerPairs) => {
return { type: 'PRELIMINARY_ANSWER_CLICKED', answerArray, correctAnswerPairs }
}

export const hideOrShowQuestion = (action,delay) => {
  return(dispatch,getState) => {
    const currentOrd = getState().question.ord;
    setTimeout(() => {
      const newOrd = getState().question.ord;
      const show_result = getState().show_result;
      if (show_result && currentOrd === newOrd) {
        dispatch({type: 'HIDE_OR_SHOW_QUESTION', action});
      }
    }, delay)
  }
}

export const correctAnswerReceived = (result,answerId) => {
  return { type: 'CORRECT_ANSWER_RECEIVED', result, answerId}
}

export const fetchPreliminaryAnswer = () => {
  console.log('lalalala')
  return (dispatch, getState) => {
    const {joinedRoomId, answerArray} = getState()
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({ 'answerPairs': answerArray })
      }


      dispatch(requestPosts())

      fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/answerid', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(data => dispatch(preliminaryAnswerClicked(answerArray, data.correctAnswerPairs)))
        //.then(setTimeout(() => dispatch(fetchQuestion()), 5000))
    }
  }
}

export const fetchAnswer = (clicked_id) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({ 'answerId': clicked_id })
      }


      const {joinedRoomId, userId, activePlayerId, normal_player_click, showPlayerAlert, showNoMorePlayerAlert} = getState()
      if (userId !== activePlayerId && !normal_player_click && !showPlayerAlert && !showNoMorePlayerAlert) {
        dispatch(normalPlayerAnswerClicked(clicked_id))
      }
      if (!normal_player_click) { //always false except when the  player has already clicked one of the answers
        fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/answerid', opts)
          .then(response => {
            if (!response.ok) {
              dispatch(check_token())
            }
          })
      }
    }
  }
}

export const normalPlayerAnswerClicked = (clicked_id,play_finalanswer) => ({
  type: 'NORMAL_PLAYER_ANSWER_CLICKED',
  clicked_id,
  play_finalanswer
})
export const playAgain = (clicked_id) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      let opts = {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({ 'answerId': clicked_id })
      }
      const {joinedRoomId} = getState()
      dispatch(requestPosts())

      fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/answerid', opts)
        .then(response => {
          if (response.ok) {
            return response.json()
          }
          else {
            dispatch(check_token())
          }
        })
        //.then(data => dispatch(answerClicked(clicked_id, data.correct)))
        .then(() => dispatch(fetchQuestion()), 3000)
    }
  }
}

export const updateRoomList = roomList => ({
  type: 'UPDATE_ROOM_LIST',
  roomList
})

export const loadRoomList = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string
    if (token) {
      console.log('fdd')
      let opts = {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }

      return fetch(rest_api_host + '/api/rooms/', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { dispatch(check_token()) }
        })
        .then(roomListJson => { dispatch(updateRoomList(roomListJson)) })
    }
  }
}

export const setJoinedRoomId = roomId => ({
  type: 'SET_JOINED_ROOM_ID',
  roomId
})

export const setPrelimResults = res => ({
  type: 'SET_PRELIIM_RESULTS',
  res
})
