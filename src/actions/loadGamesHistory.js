import { rest_api_host } from '../index'

export const setGamesHistory = gamesHistory => ({
  type: 'SET_GAMES_HISTORY',
  gamesHistory
})

const loadGamesHistory = userId => {
  return (dispatch, getState) => {
    return fetch(rest_api_host + '/api/users/' + getState().userId + '/games_history')
      .then(response => {
        if (response.ok) { return response.json() }
      })
      .then(gamesHistory => {
        dispatch(setGamesHistory(gamesHistory))
      })
  }
}

export default loadGamesHistory
