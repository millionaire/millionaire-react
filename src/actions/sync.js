export const syncStart = () => {
  return {
    type: 'SYNC_START',
  }
}

export const syncComplete = () => {
  return {
    type: 'SYNC_COMPLETE',
  }
}
