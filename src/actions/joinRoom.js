import { rest_api_host } from '../index'
import { check_token } from './auth'
import { setJoinedRoomId ,getOneRoom} from './game_fetches'
import { stopGame } from './startGame'

const joinRoom = (roomId) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      
      console.log('ROOM_ID:', roomId)
      
      let opts = { 
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({
          'password': '',
        }),
      }

      /*eslint-disable */
      let joinedRoomId = getState().joinedRoomId
      if (joinedRoomId) {
        if (confirm(`Do you wanna quit room ${joinedRoomId}?`)) {
          dispatch(stopGame())
          dispatch(setJoinedRoomId(null))
        }
        else {
          return
        }
      }
      /*eslint-enable */
      dispatch(setJoinedRoomId(roomId));
      return fetch(rest_api_host + '/api/rooms/' + roomId + '/join', opts)
        .then(response => {
          if (response.ok) { 
            return response.json();
           }
          else { dispatch(check_token()) }          
        })
        .then(room => {
          dispatch(getOneRoom(room));
        })
    }
  }
}

export default joinRoom
