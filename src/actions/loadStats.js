import { rest_api_host } from '../index'

export const setStats = stats => {
  return {
    type: 'SET_STATS',
    stats,
  }
}


const loadStats = roomId => {
  return (dispatch, getState) => {
    return fetch(rest_api_host + `/api/rooms/${getState().joinedRoomId}/stats`)
      .then(response => {
      if (response.ok) { 
        return response.json() 
        }
      })
      .then(stats => {
        console.log(stats)
        dispatch(setStats(stats))
      })
  }
}

export default loadStats
