import { rest_api_host } from '../index'
import { check_token } from './auth'

export const setInviteUrl = tempRoomUrl => {
  return {
    type: 'SET_INVITE_URL',
    tempRoomUrl,
  }
}


const enableInvite = roomId => {
  return dispatch => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { method: 'GET', headers: { 'access_token': token }}

      return fetch(rest_api_host + `/api/rooms/${roomId}/enable_invite`, opts)
        .then(response => {
          if (response.ok) { 
            return response.text() 
          }
          else { 
            dispatch(check_token()) 
          }
        })
        .then(shortUrl => {
          // console.log(shortUrl)
          dispatch(setInviteUrl(shortUrl.slice(8)))
        })
          // dispatch(updateRoomList(getState().roomList.concat(room)))
          // dispatch(syncComplete())
          // dispatch(redirectToRoom(room.id))
          // dispatch(syncComplete())
          // dispatch(redirectToRoom(null))
          // dispatch(getOneRoom(room));
        // })
    }
  }
}

export default enableInvite
