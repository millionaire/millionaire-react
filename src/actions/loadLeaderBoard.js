import { rest_api_host } from '../index'

export const setLeaderBoard = leaderBoard => ({
  type: 'SET_LEADERBOARD',
  leaderBoard
})

const loadLeaderBoard = (roomId) => {
  return dispatch => {
    return fetch(rest_api_host + '/api/users')
      .then(response => {
        if (response.ok) { return response.json() }
      })
      .then(leaderBoard => {
        dispatch(setLeaderBoard(leaderBoard))
      })
  }
}

export default loadLeaderBoard
