const setGameStatus = status => ({
  type: 'SET_GAME_STATUS',
  status,
})
export default setGameStatus
