import { rest_api_host } from '../index'

export const setVoiceCommands = voiceCommands => {
  return {
    type: 'SET_VOICE_COMMANDS',
    voiceCommands,
  }
}

export const setVoicePhrases = voicePhrases => {
  return {
    type: 'SET_VOICE_PHRASES',
    voicePhrases,
  }
}

export const loadVoiceCommands = () => {
  return dispatch => {
    let token = localStorage.getItem('access_token')
    if (token) {
      let opts = {
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }
      return fetch(rest_api_host + '/api/voice/actions', opts)
        .then(response => {
          if (response.ok) { return response.json() }
        })
        .then(voiceCommands => {
          dispatch(setVoiceCommands(voiceCommands))
        })
    }
  }
}

export const loadVoicePhrases = () => {
  return dispatch => {
    let token = localStorage.getItem('access_token')
    if (token) {
      let opts = {
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }
      return fetch(rest_api_host + '/api/voice/phrases', opts)
        .then(response => {
          if (response.ok) { return response.json() }
        })
        .then(VoicePhrases => {
          dispatch(setVoicePhrases(VoicePhrases))
        })
    }
  }
}

export const updateVoiceCommands = voiceCommands => {
  return dispatch => {
    let token = localStorage.getItem('access_token')
    if (token) {
      let opts = {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify(voiceCommands)
      }
      return fetch(rest_api_host + '/api/voice/update', opts)
        .then(response => {
          if (response.ok) {
            dispatch(loadVoicePhrases())
            alert('сохранено')
          }
        })
    }
  }
}
